import pytest

from libs.testdata.admin_data import AdminData


@pytest.fixture
def admin_data():
    return AdminData().return_data()
