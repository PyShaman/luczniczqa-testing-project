import pytest

from assertpy import assert_that

from libs.config import Config
from libs.crud_methods import create


@pytest.fixture
def access_token():
    at = create(f"{Config.URL}/admin/login",
                headers={
                    "accept": "application/json",
                    "Content-Type": "application/json"
                },
                json={
                    "username": Config.EMAIL,
                    "password": Config.PASSWORD
                })
    assert_that(at.status_code).is_equal_to(200)
    return at.json()["data"][0]["access_token"]
