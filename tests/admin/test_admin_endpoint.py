from time import time

import pytest

from assertpy import assert_that, soft_assertions
from datetime import timedelta
from uuid import UUID

from libs.config import Config
from libs.crud_methods import create, update


@pytest.mark.admin
class TestAdmin:

    def test_01_create_access_token_response_200(self):
        response = create(f"{Config.URL}/admin/login",
                          headers={
                              "accept": "application/json",
                              "Content-Type": "application/json"
                          },
                          json={
                              "username": Config.EMAIL,
                              "password": Config.PASSWORD
                          })
        with soft_assertions():
            # Verify status code
            assert_that(response.status_code).is_equal_to(200)
            # Verify response payload
            assert_that(response.json()["data"][0]).contains_key("access_token")
            # Verify headers
            assert_that(response.headers["content-type"]).is_equal_to("application/json")
            assert_that(UUID(response.headers.get("x-luczniczqa"), version=4)).is_true()
            assert_that(response.headers["server"]).is_equal_to("uvicorn")
            # Verify basic performance sanity
            assert_that(response.elapsed).is_less_than_or_equal_to(timedelta(milliseconds=500))

    def test_02_create_access_token_response_401_invalid_username(self):
        response = create(f"{Config.URL}/admin/login",
                          headers={
                              "accept": "application/json",
                              "Content-Type": "application/json"
                          },
                          json={
                              "username": "invalid_username",
                              "password": Config.PASSWORD
                          })
        with soft_assertions():
            # Verify status code
            assert_that(response.status_code).is_equal_to(401)
            # Verify response payload
            assert_that(response.json()).contains_value("Incorrect email or password")
            # Verify headers
            assert_that(response.headers["content-type"]).is_equal_to("application/json")
            assert_that(UUID(response.headers.get("x-luczniczqa"), version=4)).is_true()
            assert_that(response.headers["server"]).is_equal_to("uvicorn")
            # Verify basic performance sanity
            assert_that(response.elapsed).is_less_than_or_equal_to(timedelta(milliseconds=500))

    def test_03_create_access_token_response_401_invalid_password(self):
        response = create(f"{Config.URL}/admin/login",
                          headers={
                              "accept": "application/json",
                              "Content-Type": "application/json"
                          },
                          json={
                              "username": Config.EMAIL,
                              "password": "incorrect_password"
                          })
        with soft_assertions():
            # Verify status code
            assert_that(response.status_code).is_equal_to(401)
            # Verify response payload
            assert_that(response.json()).contains_value("Incorrect email or password")
            # Verify headers
            assert_that(response.headers["content-type"]).is_equal_to("application/json")
            assert_that(UUID(response.headers.get("x-luczniczqa"), version=4)).is_true()
            assert_that(response.headers["server"]).is_equal_to("uvicorn")
            # Verify basic performance sanity
            assert_that(response.elapsed).is_less_than_or_equal_to(timedelta(milliseconds=500))

    def test_04_create_access_token_response_422_missing_username_field(self):
        response = create(f"{Config.URL}/admin/login",
                          headers={
                              "accept": "application/json",
                              "Content-Type": "application/json"
                          },
                          json={
                              "password": Config.PASSWORD,
                          })
        with soft_assertions():
            # Verify status code
            assert_that(response.status_code).is_equal_to(422)
            # Verify response payload
            assert_that(response.json()["detail"][0]["loc"]).contains("username")
            # Verify headers
            assert_that(response.headers["content-type"]).is_equal_to("application/json")
            assert_that(response.headers["server"]).is_equal_to("uvicorn")
            # Verify basic performance sanity
            assert_that(response.elapsed).is_less_than_or_equal_to(timedelta(milliseconds=500))

    def test_05_create_access_token_response_422_missing_password_field(self):
        response = create(f"{Config.URL}/admin/login",
                          headers={
                              "accept": "application/json",
                              "Content-Type": "application/json"
                          },
                          json={
                              "username": Config.EMAIL,
                          })
        with soft_assertions():
            # Verify status code
            assert_that(response.status_code).is_equal_to(422)
            # Verify response payload
            assert_that(response.json()["detail"][0]["loc"]).contains("password")
            # Verify headers
            assert_that(response.headers["content-type"]).is_equal_to("application/json")
            assert_that(response.headers["server"]).is_equal_to("uvicorn")
            # Verify basic performance sanity
            assert_that(response.elapsed).is_less_than_or_equal_to(timedelta(milliseconds=500))

    def test_06_create_account_response_200_valid_data(self, admin_data):
        response = create(f"{Config.URL}/admin/",
                          headers={
                              "accept": "application/json",
                              "Content-Type": "application/json"
                          },
                          json=admin_data)
        with soft_assertions():
            # Verify status code
            assert_that(response.status_code).is_equal_to(200)
            # Verify response payload
            assert_that(response.json()["id"]).is_not_none()
            assert_that(response.json()["fullname"]).is_equal_to(admin_data["fullname"])
            assert_that(response.json()["email"]).is_equal_to(admin_data["email"])
            # Verify headers
            assert_that(response.headers["content-type"]).is_equal_to("application/json")
            assert_that(UUID(response.headers.get("x-luczniczqa"), version=4)).is_true()
            assert_that(response.headers["server"]).is_equal_to("uvicorn")
            # Verify basic performance sanity
            assert_that(response.elapsed).is_less_than_or_equal_to(timedelta(milliseconds=500))

    def test_07_create_account_response_409_existing(self):
        response = create(f"{Config.URL}/admin/",
                          headers={
                              "accept": "application/json",
                              "Content-Type": "application/json"
                          },
                          json={
                              "fullname": Config.FULLNAME,
                              "email": Config.EMAIL,
                              "password": Config.PASSWORD
                          })
        with soft_assertions():
            # Verify status code
            assert_that(response.status_code).is_equal_to(409)
            # Verify response payload
            assert_that(response.json()["detail"]).is_equal_to("Email already exists")
            # Verify headers
            assert_that(response.headers["content-type"]).is_equal_to("application/json")
            assert_that(UUID(response.headers.get("x-luczniczqa"), version=4)).is_true()
            assert_that(response.headers["server"]).is_equal_to("uvicorn")
            # Verify basic performance sanity
            assert_that(response.elapsed).is_less_than_or_equal_to(timedelta(milliseconds=500))

    def test_08_create_account_response_422_missing_fullname(self, admin_data):
        response = create(f"{Config.URL}/admin/",
                          headers={
                              "accept": "application/json",
                              "Content-Type": "application/json"
                          },
                          json={
                              "email": admin_data["email"],
                              "password": admin_data["password"]
                          })
        with soft_assertions():
            # Verify status code
            assert_that(response.status_code).is_equal_to(422)
            # Verify response payload
            assert_that(response.json()["detail"][0]["loc"]).contains("fullname")
            # Verify headers
            assert_that(response.headers["content-type"]).is_equal_to("application/json")
            assert_that(response.headers["server"]).is_equal_to("uvicorn")
            # Verify basic performance sanity
            assert_that(response.elapsed).is_less_than_or_equal_to(timedelta(milliseconds=500))

    def test_09_create_account_response_422_missing_email(self, admin_data):
        response = create(f"{Config.URL}/admin/",
                          headers={
                              "accept": "application/json",
                              "Content-Type": "application/json"
                          },
                          json={
                              "fullname": admin_data["fullname"],
                              "password": admin_data["password"]
                          })
        with soft_assertions():
            # Verify status code
            assert_that(response.status_code).is_equal_to(422)
            # Verify response payload
            assert_that(response.json()["detail"][0]["loc"]).contains("email")
            # Verify headers
            assert_that(response.headers["content-type"]).is_equal_to("application/json")
            assert_that(response.headers["server"]).is_equal_to("uvicorn")
            # Verify basic performance sanity
            assert_that(response.elapsed).is_less_than_or_equal_to(timedelta(milliseconds=500))

    def test_10_create_account_response_422_missing_password(self, admin_data):
        response = create(f"{Config.URL}/admin/",
                          headers={
                              "accept": "application/json",
                              "Content-Type": "application/json"
                          },
                          json={
                              "fullname": admin_data["fullname"],
                              "email": admin_data["email"]
                          })
        with soft_assertions():
            # Verify status code
            assert_that(response.status_code).is_equal_to(422)
            # Verify response payload
            assert_that(response.json()["detail"][0]["loc"]).contains("password")
            # Verify headers
            assert_that(response.headers["content-type"]).is_equal_to("application/json")
            assert_that(response.headers["server"]).is_equal_to("uvicorn")
            # Verify basic performance sanity
            assert_that(response.elapsed).is_less_than_or_equal_to(timedelta(milliseconds=500))

    def test_11_read_access_token_response_405(self):
        response = update(f"{Config.URL}/admin/login",
                          headers={
                              "accept": "application/json",
                              "Content-Type": "application/json"
                          },
                          json={
                              "username": Config.EMAIL,
                              "password": Config.PASSWORD
                          })
        with soft_assertions():
            # Verify status code
            assert_that(response.status_code).is_equal_to(405)
            # Verify response payload
            assert_that(response.json()["detail"]).is_equal_to("Method Not Allowed")
            # Verify headers
            assert_that(response.headers["content-type"]).is_equal_to("application/json")
            assert_that(response.headers["server"]).is_equal_to("uvicorn")
            # Verify basic performance sanity
            assert_that(response.elapsed).is_less_than_or_equal_to(timedelta(milliseconds=500))

    def test_12_create_account_and_retrieve_access_token(self):
        payload = {
            "fullname": f"test{time()}",
            "email": f"test{time()}@gmail.com",
            "password": "9{LBupmwc*jVN^kB"
        }
        admin = create(f"{Config.URL}/admin/",
                       headers={
                           "accept": "application/json",
                           "Content-Type": "application/json"
                       },
                       json=payload)
        token = create(f"{Config.URL}/admin/login",
                       headers={
                           "accept": "application/json",
                           "Content-Type": "application/json"
                       },
                       json={
                           "username": payload["email"],
                           "password": payload["password"]
                       })
        with soft_assertions():
            # Verify status code
            assert_that(admin.status_code).is_equal_to(200)
            assert_that(token.status_code).is_equal_to(200)
            # Verify response payload
            assert_that(admin.json()["id"]).is_not_none()
            assert_that(admin.json()["fullname"]).is_equal_to(payload["fullname"])
            assert_that(admin.json()["email"]).is_equal_to(payload["email"])
            assert_that(token.json()["data"][0]).contains_key("access_token")
            # Verify headers
            assert_that(admin.headers["content-type"]).is_equal_to("application/json")
            assert_that(UUID(admin.headers.get("x-luczniczqa"), version=4)).is_true()
            assert_that(admin.headers["server"]).is_equal_to("uvicorn")
            assert_that(token.headers["content-type"]).is_equal_to("application/json")
            assert_that(UUID(token.headers.get("x-luczniczqa"), version=4)).is_true()
            assert_that(token.headers["server"]).is_equal_to("uvicorn")
            # Verify basic performance sanity
            assert_that(admin.elapsed).is_less_than_or_equal_to(timedelta(milliseconds=500))
            assert_that(token.elapsed).is_less_than_or_equal_to(timedelta(milliseconds=500))
