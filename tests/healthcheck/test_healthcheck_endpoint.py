import pytest

from assertpy import assert_that, soft_assertions
from datetime import timedelta, datetime
from dateutil.parser import parse
from uuid import UUID

from libs.config import Config
from libs.crud_methods import read, create


@pytest.mark.health
class TestHealthcheck:

    def test_read_healthcheck_response_200(self):
        response = read(f"{Config.URL}/healthcheck",
                        headers={
                            "accept": "application/json",
                            "Content-Type": "application/json"
                        })
        with soft_assertions():
            # Verify status code
            assert_that(response.status_code).is_equal_to(200)
            # Verify response payload
            assert_that(parse(response.json()["current_date"])).is_less_than_or_equal_to(datetime.now())
            assert_that(response.json()["luczniczqa_online"]).is_true()
            # Verify headers
            assert_that(response.headers["content-type"]).is_equal_to("application/json")
            assert_that(UUID(response.headers.get("x-luczniczqa"), version=4)).is_true()
            assert_that(response.headers["server"]).is_equal_to("uvicorn")
            # Verify basic performance sanity
            assert_that(response.elapsed).is_less_than_or_equal_to(timedelta(milliseconds=500))

    def test_02_create_healthcheck_response_405(self):
        response = create(f"{Config.URL}/healthcheck",
                          headers={
                              "accept": "application/json",
                              "Content-Type": "application/json"
                          })
        with soft_assertions():
            # Verify status code
            assert_that(response.status_code).is_equal_to(405)
            # Verify response payload
            assert_that(response.json()["detail"]).is_equal_to("Method Not Allowed")
            # Verify headers
            assert_that(response.headers["content-type"]).is_equal_to("application/json")
            assert_that(response.headers["server"]).is_equal_to("uvicorn")
            # Verify basic performance sanity
            assert_that(response.elapsed).is_less_than_or_equal_to(timedelta(milliseconds=500))
