from datetime import timedelta

import pytest

from assertpy import assert_that, soft_assertions
from uuid import UUID

from libs.config import Config
from libs.crud_methods import read, create


@pytest.mark.status
class TestStatus:

    def test_01_create_status_response_200(self):
        response = create(f"{Config.URL}/status/",
                          headers={
                              "accept": "application/json",
                              "Content-Type": "application/json"
                          },
                          json={
                              "status": "OFFLINE"
                          })
        with soft_assertions():
            # Verify status code
            assert_that(response.status_code).is_equal_to(200)
            # Verify response payload
            assert_that(response.json()["data"][0]["status"]).is_equal_to("Welcome to ŁuczniczQA API Testing app.")
            # Verify headers
            assert_that(response.headers["content-type"]).is_equal_to("application/json")
            assert_that(response.headers["server"]).is_equal_to("uvicorn")
            # Verify basic performance sanity
            assert_that(response.elapsed).is_less_than_or_equal_to(timedelta(milliseconds=500))
