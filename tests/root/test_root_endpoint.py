import pytest

from assertpy import assert_that, soft_assertions
from datetime import timedelta

from libs.config import Config
from libs.crud_methods import read, create


@pytest.mark.root
class TestRoot:

    def test_01_read_root_response_200(self):
        response = read(f"{Config.URL}/",
                        headers={
                            "accept": "application/json",
                            "Content-Type": "application/json"
                        })
        with soft_assertions():
            # Verify status code
            assert_that(response.status_code).is_equal_to(200)
            # Verify response payload
            assert_that(response.json()["message"]).is_equal_to("Welcome to ŁuczniczQA API Testing app.")
            # Verify headers
            assert_that(response.headers["content-type"]).is_equal_to("application/json")
            assert_that(response.headers["server"]).is_equal_to("uvicorn")
            # Verify basic performance sanity
            assert_that(response.elapsed).is_less_than_or_equal_to(timedelta(milliseconds=500))

    def test_02_create_root_response_405(self):
        response = create(f"{Config.URL}/",
                          headers={
                              "accept": "application/json",
                              "Content-Type": "application/json"
                          })
        with soft_assertions():
            # Verify status code
            assert_that(response.status_code).is_equal_to(405)
            # Verify response payload
            assert_that(response.json()["detail"]).is_equal_to("Method Not Allowed")
            # Verify headers
            assert_that(response.headers["content-type"]).is_equal_to("application/json")
            assert_that(response.headers["server"]).is_equal_to("uvicorn")
            # Verify basic performance sanity
            assert_that(response.elapsed).is_less_than_or_equal_to(timedelta(milliseconds=500))
